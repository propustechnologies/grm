﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using GRM.Common.Entities;
using GRM.Common.Infrastructure;
using GRM.Common.Models;
using GRM.Common.Parsers.Date;

namespace GRM.Common.Parsers.Data
{
    public class DataParser<T> : IDataParser<T> where T :  Entity 
    {
        private readonly GetDateParserArgs _args;
        private readonly string[] _headers;
        private readonly Dictionary<string, PropertyInfo> _properties;
        private IDateParserFactory _factory => new DateParserFactory();

        public DataParser(GetDateParserArgs args, string[] headers)
        {
            _args = args;
            _headers = headers;
            _properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(info => info.Name);
        }
        
        /// <summary>
        /// Those ifs smell fishy but it is unavoidable of working with reflection for this test :/
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public T GetEntity(string line)
        {
            string[] lineData = line.Split('|');
            T obj = Activator.CreateInstance<T>();
            for (int i = 0; i < _headers.Length; i++)
            {
                PropertyInfo pinfo = _properties[_headers[i]];
                object value = null;
                if (pinfo.PropertyType == typeof(DateTime) || pinfo.PropertyType == typeof(DateTime?))
                {
                    //I have to need the original values so I have to keep in a property with String suffix.
                    _properties[_headers[i] + "String"].SetValue(obj, lineData[i], null);
                    
                    if (!string.IsNullOrWhiteSpace(lineData[i]))
                    {
                        _args.DateString = lineData[i];      
                        IDateParser dateParser = _factory.GetDateParser(_args);
                        value = dateParser.ParseDate(lineData[i]);
                    }
                }
                else if (pinfo.PropertyType == typeof(IEnumerable<string>))
                {
                    value = lineData[i].Split(',');
                }
                else
                {
                    value = lineData[i];
                }
                pinfo.SetValue(obj, value, null);                
            }
            return obj;
        }
    }
}