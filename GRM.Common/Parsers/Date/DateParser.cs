﻿using System;
using GRM.Common.Infrastructure;
using GRM.Common.Models;

namespace GRM.Common.Parsers.Date
{
    /// <summary>
    /// Base DateParser Class. 
    /// </summary>
    public abstract class DateParser : IDateParser
    {
        private readonly DateParserHelper _dateParserHelper;

        protected DateParser(string cultureCode, string dateFormatRegex)
        {
            _dateParserHelper = new DateParserHelper(new DateParserHelperArgs
            {
                CultureCode =  cultureCode,
                DayFormatRegex = dateFormatRegex,
                DateFormat = this.DateFormat
            });
        }

        public virtual DateTime ParseDate(string dateString)
        {
            return _dateParserHelper.ParseDate(dateString);
        }

        /// <summary>
        /// Read-only property of implemented Parser.
        /// </summary>
        public abstract string DateFormat { get; }
    }
}