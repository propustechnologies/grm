﻿namespace GRM.Common.Parsers.Date
{
    public static class DateParserHelperExtensionMethods
    {
        /// <summary>
        /// Check date string has proper length
        /// </summary>
        /// <param name="dateData"></param>
        /// <returns></returns>
        public static bool CheckDateStringParts(this string[] dateData)
        {
            return dateData.Length != 3;
        }

        /// <summary>
        /// Split date string
        /// </summary>
        /// <param name="dateString"></param>
        /// <returns></returns>
        public static string[] SplitDateString(this string dateString)
        {
            string[] dateData = dateString.Split(' ');
            return dateData;
        }
    }
}