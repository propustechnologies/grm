﻿namespace GRM.Common.Parsers.Date
{
    /// <summary>
    /// Implementation for MMMM format dates
    /// </summary>
    public class LongMonthDateParser : DateParser
    {
        public override string DateFormat => "d MMMM yyyy";

        public LongMonthDateParser(string cultureCode, string dateFormatRegex) : base(cultureCode, dateFormatRegex)
        {
            
        }
    }
}