﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using GRM.Common.Exceptions;
using GRM.Common.Infrastructure;
using GRM.Common.Models;

namespace GRM.Common.Parsers.Date
{
   
    /// <summary>
    /// The Date Parser Helper
    /// </summary>
    public class DateParserHelper : IDateParserHelper
    {
        private Regex dayFormatRegex;
        private IFormatProvider culture;
        private string dateFormat;

        public DateParserHelper(DateParserHelperArgs args)
        {
            dayFormatRegex = new Regex(args.DayFormatRegex);
            culture = new CultureInfo(args.CultureCode);
            dateFormat = args.DateFormat;
        }
        /// <summary>
        /// Date parser
        /// </summary>
        /// <param name="dateString">Date string</param>
        /// <returns>date</returns>
        public DateTime ParseDate(string dateString)
        {
            var fixedDateString = GetFixedDateString(dateString);
            DateTime date = DateTime.MinValue;
            var result = DateTime.TryParseExact(fixedDateString, dateFormat, culture, DateTimeStyles.None, out date);
            if(!result)
                ThrowInvalidDateException();
            
            return date;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dayString">Day part of given date string.</param>
        /// <returns>integer value</returns>
        /// <exception cref="InvalidDayFormatException"></exception>
        private int ParseDay(string dayString)
        {
            if (dayFormatRegex.IsMatch(dayString))
                return Int16.Parse(dayString.Remove(dayString.Length - 2));
            throw new InvalidDayFormatException("Invalid day format. Example : 1st, 2nd, 3th");
        }

        /// <summary>
        /// Fixes given date string to compitable string to use in DateTime.TryParseExact.
        /// </summary>
        /// <param name="dateString"></param>
        /// <returns></returns>
        private string GetFixedDateString(string dateString)
        {
            var dateData = dateString.SplitDateString();
            if (dateData.CheckDateStringParts())
            {
                ThrowInvalidDateException();
            }
            var day = ParseDay(dateData[0]);
            dateData[0] = day.ToString();
            var fixedDateString = string.Join(" ", dateData);
            return fixedDateString;
        }

        /// <summary>
        /// to prevent using repeating Magic String. (DRY) 
        /// </summary>
        /// <param name="message"></param>
        /// <exceptio cref="InvalidInputDateFormatException"></exception>
        private void ThrowInvalidDateException(string message = "Invalid Date Format. Example : 3rd September 2017")
        {
            throw new InvalidInputDateFormatException(message);
        }
    }
}