﻿namespace GRM.Common.Parsers.Date
{
    /// <summary>
    /// Implementation of MMM format dates
    /// </summary>
    public class ShortMonthDateParser: DateParser
    {
        public override string DateFormat => "d MMM yyyy";

        public ShortMonthDateParser(string cultureCode, string dateFormatRegex) : base(cultureCode, dateFormatRegex)
        {
        }
    }
}