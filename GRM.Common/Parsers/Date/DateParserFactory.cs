﻿using System;
using System.Collections.Generic;
using GRM.Common.Exceptions;
using GRM.Common.Infrastructure;
using GRM.Common.Models;

namespace GRM.Common.Parsers.Date
{
    public class DateParserFactory : IDateParserFactory
    {
        //Singleton for each DateParserFactory
        //If this factory will registered as singleton in DI than it will become app wide singleton parser list.
        //DateParser can solve this by Month info but this is a demostration of Factory pattern and Singleton.
        private readonly Dictionary<string , IDateParser> _parsers = new Dictionary<string, IDateParser>();
        
        public IDateParser GetDateParser(GetDateParserArgs args)
        {
            var dateData = args.DateString.SplitDateString();
            if (dateData.CheckDateStringParts())
            {
                throw new InvalidInputDateFormatException();
            }
            
            switch (dateData[1].Length)
            {
                case 3:
                    return GetInstance("short", typeof(ShortMonthDateParser), args.CultureCode, args.DayFormatRegex);
                default: 
                    return GetInstance("long", typeof(LongMonthDateParser), args.CultureCode, args.DayFormatRegex); 
            }
        }

        private IDateParser GetInstance(string key, Type type, string cultureCode, string dayFormatRegex)
        {
            if (!_parsers.ContainsKey(key))
            {
                var instance = (IDateParser) Activator.CreateInstance(type, cultureCode, dayFormatRegex);
                _parsers.Add(key,  instance);
            }
            return _parsers[key];
        }
    }
}