﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using GRM.Common.Entities;
using GRM.Common.Models;
using GRM.Common.Parsers.Data;

namespace GRM.Common.Data
{
    /// <summary>
    /// Singleton Data Context.
    /// </summary>
    public static class DataContext
    {
        private static Hashtable _collections = new Hashtable();

        
        /// <summary>
        /// Entry point of datacontext. Loads and indexes the collection.
        /// </summary>
        /// <param name="fileRef"></param>
        /// <typeparam name="T"></typeparam>
        public static  void LoadData<T>(string fileRef) where T : Entity
        {
            string typeName = typeof(T).FullName;
            if (!_collections.ContainsKey(typeName))
            {
                _collections.Add(typeName, LoadDataFromFile<T>(fileRef));
            }
        }
        
        /// <summary>
        /// Get Requested Collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<T> GetCollection<T>() where T : Entity
        {
            var collection = _collections[typeof(T).FullName];
            return (Collection<T>) collection;
        }

        /// <summary>
        /// Loads entities.
        /// </summary>
        /// <param name="fileRef"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private static IReadOnlyCollection<T> LoadDataFromFile<T>(string fileRef) where  T : Entity
        {
            var collection = new Collection<T>();
            using (FileStream fs = new FileStream(fileRef, FileMode.Open))
            {
                using (StreamReader reader = new StreamReader(fs))
                {
                    string[] headers = reader.ReadLine().Split('|');
                    
                    string line = null;
                    GetDateParserArgs getDateParserArgs = new GetDateParserArgs
                    {
                        DayFormatRegex = @"^\d{1,2}((st)|(nd)|(rd)|(th))$",
                        CultureCode = "en-GB"
                    };
                    
                    while ((line = reader.ReadLine()) != null)
                    {
                        DataParser<T> dataParser = new DataParser<T>(getDateParserArgs, headers);
                        var entityInstance = dataParser.GetEntity(line);
                        collection.Add(entityInstance);
                    }
                }
            }
            return collection;
        }
    }
}