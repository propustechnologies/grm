﻿namespace GRM.Common.Exceptions
{
    public class InvalidDayFormatException : InvalidInputDateFormatException
    {
        public InvalidDayFormatException(string message) : base(message)
        {
        }
    }
}