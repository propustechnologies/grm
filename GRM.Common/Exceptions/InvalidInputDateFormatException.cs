﻿using System;

namespace GRM.Common.Exceptions
{
    public class InvalidInputDateFormatException : FormatException
    {
        public InvalidInputDateFormatException(string message = "Invalid Date Format. Example : 3rd September 2017") : base(message)
        {
            
        }
    }
}