﻿using System;

namespace GRM.Common.Infrastructure
{
    public interface IDateParser
    {
        string DateFormat { get; }
        DateTime ParseDate(string dateString);
    }
}