﻿using GRM.Common.Entities;

namespace GRM.Common.Infrastructure
{
    public interface IDataParser<T> where T : Entity
    {
        /// <summary>
        /// Those ifs smell fishy but it is unavoidable of working with reflection for this test :/
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        T GetEntity(string line);
    }
}