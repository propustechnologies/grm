﻿using System.Collections.Generic;

namespace GRM.Common.Infrastructure
{
    public interface IRepository<T>
    {
        IEnumerable<T> DbSet { get; }
    }
}