﻿using GRM.Common.Models;

namespace GRM.Common.Infrastructure
{
    public interface IDateParserFactory
    {
        IDateParser GetDateParser(GetDateParserArgs args);
    }
}