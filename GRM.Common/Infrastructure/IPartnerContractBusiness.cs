﻿using System.Collections.Generic;
using GRM.Common.Entities;

namespace GRM.Common.Infrastructure
{
    public interface IPartnerContractBusiness
    {
        IEnumerable<PartnerContract> GetContractsByPartnerName(string requestPartnerName);
    }
}