﻿using System.Collections.Generic;
using GRM.Common.Messages;

namespace GRM.Common.Infrastructure
{
    public interface ICommonService
    {
        IEnumerable<GetMusicContractsByDateAndPartnerResponse> GetMusicContractsByDateAndPartner(GetMusicContractsByDateAndPartnerNameRequest request);
    }
}