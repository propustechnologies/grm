﻿using System;
using System.Collections.Generic;
using GRM.Common.Entities;

namespace GRM.Common.Infrastructure
{
    public interface IMusicContractBusiness
    {
        IEnumerable<MusicContract> GetContractsByDate(DateTime requestDate);
    }
}