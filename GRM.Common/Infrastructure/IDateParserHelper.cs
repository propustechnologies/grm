﻿using System;
using GRM.Common.Exceptions;

namespace GRM.Common.Infrastructure
{
    public interface IDateParserHelper
    {
        DateTime ParseDate(string dateString);
    }
}