﻿using System.Collections.Generic;
using GRM.Common.Entities;

namespace GRM.Common.Infrastructure
{
    public interface IPartnerContractsRepository
    {
        IEnumerable<PartnerContract> GetByUsage(string usage);
        IEnumerable<PartnerContract> GetByPartner(string partnerName);
    }
}