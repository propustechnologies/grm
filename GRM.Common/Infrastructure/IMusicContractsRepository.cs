﻿using System;
using System.Collections.Generic;
using GRM.Common.Entities;

namespace GRM.Common.Infrastructure
{
    public interface IMusicContractsRepository
    {
        IEnumerable<MusicContract> GetByDate(DateTime date);
    }
}