﻿namespace GRM.Common.Messages
{
    public class GetMusicContractsByDateAndPartnerNameRequest
    {
        public string PartnerName { get; set; }
        public string DateString { get; set; }
    }
}