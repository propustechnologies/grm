﻿using System.Collections.Generic;
using GRM.Common.Entities;

namespace GRM.Common.Messages
{
    /// <summary>
    /// Encapsulation of response message.
    /// </summary>
    public class GetMusicContractsByDateAndPartnerResponse
    {
        public string Artist { get; set; }
        public string Title { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Usage { get; set; }
    }
}