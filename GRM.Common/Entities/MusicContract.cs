﻿using System;
using System.Collections.Generic;

namespace GRM.Common.Entities
{
    public class MusicContract : Entity
    {
        public string Artist { get; set; }
        public string Title { get; set; }
        public IEnumerable<string> Usages { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        
        public string StartDateString { get; set; }
        public string EndDateString { get; set; }
        
    }
}