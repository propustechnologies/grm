﻿namespace GRM.Common.Entities
{
    public class PartnerContract : Entity
    {
        public string Partner { get; set; }
        public string Usage { get; set; }
    }
}