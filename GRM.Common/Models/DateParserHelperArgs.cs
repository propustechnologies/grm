﻿namespace GRM.Common.Models
{
    public class DateParserHelperArgs
    {
        public string DayFormatRegex { get; set; }
        public string CultureCode { get; set; }
        public string DateFormat { get; set; }
    }
}