﻿namespace GRM.Common.Models
{
    public class GetDateParserArgs
    {
        public string DateString { get; set; }
        public string CultureCode { get; set; }
        public string DayFormatRegex { get; set; }
    }
}