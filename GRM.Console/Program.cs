﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GRM.Common.Data;
using GRM.Common.Entities;
using GRM.Common.Infrastructure;
using GRM.Common.Messages;
using GRM.Service;

namespace GRM.Console
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                string partnerContractsFilePath = GetDataFromConsole("Please enter Partner Contracts File Path : ");
                DataContext.LoadData<PartnerContract>(partnerContractsFilePath);

                string musicContractsFilePath = GetDataFromConsole("Please enter Music Contracts File Path : ");
                DataContext.LoadData<MusicContract>(musicContractsFilePath);

                string searchArgumentString = GetDataFromConsole("Please enter search criteria: ");
                GetMusicContractsByDateAndPartnerNameRequest request = GetArguments(searchArgumentString);

                var commonService = new CommonService();

                var result = commonService.GetMusicContractsByDateAndPartner(request).OrderBy(n=>n.Artist);
                System.Console.WriteLine("Artist|Title|Usages|StartDate|EndDate");
                foreach (var item in result)
                {
                    System.Console.WriteLine($"{item.Artist}|{item.Title}|{item.Usage}|{item.StartDate}|{item.EndDate}");
                }
            }
            catch (FormatException ex)
            {
                System.Console.WriteLine(ex.Message);
            }
            catch (FileNotFoundException ex)
            {
                System.Console.WriteLine(ex.Message);
            }       

            System.Console.ReadKey();
        }

        private static GetMusicContractsByDateAndPartnerNameRequest GetArguments(string searchArgumentString)
        {
            if (string.IsNullOrWhiteSpace(searchArgumentString))
                ThrowInvalidInputFormatException();
            
            var data = searchArgumentString.Split(' ');
            
            if(data.Length != 4)
                ThrowInvalidInputFormatException();
            
            return new GetMusicContractsByDateAndPartnerNameRequest
            {
                PartnerName = data[0],
                DateString = string.Join((" "), data.Skip(1))
            };
        }

        private static void ThrowInvalidInputFormatException()
        {
            throw new FormatException("Incorrect Input Format. Please enter like YouTube 27th Dec 2012");
        }

        private static string GetDataFromConsole(string message)
        {
            System.Console.Write(message);
            return System.Console.ReadLine();
        }
    }
}