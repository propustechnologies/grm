﻿using System;
using GRM.Business;
using GRM.Common.Exceptions;
using GRM.Common.Infrastructure;
using GRM.Common.Models;
using GRM.Common.Parsers.Date;
using NUnit.Framework;

namespace GRM.Tests
{
    [TestFixture]
    public class DateHelperTests
    {
        private DateParserHelper _dateParserHelper;
        private DateParserHelperArgs _dateParserHelperArgs;
        private DateParserFactory _dateParserFactory;
        [SetUp]
        public void SetUp()
        {
            _dateParserHelperArgs = new DateParserHelperArgs
        {
            DayFormatRegex = @"^\d{1,2}((st)|(nd)|(rd)|(th))$",
            CultureCode = "en-GB",
            DateFormat = "dd MMMM yyyy"
        };
            _dateParserHelper = new DateParserHelper(_dateParserHelperArgs);
            _dateParserFactory = new DateParserFactory();
        }
        
        [Test]
        public void Test_Throw_InputDateFormatException()
        {
            string exampleDate = "27th Dec 2012";
            void TestDelegate() => _dateParserHelper.ParseDate(exampleDate);
            Assert.Throws<InvalidInputDateFormatException>(TestDelegate);
        }
        
        [Test]
        public void Test_Throw_DayOfInputDateFormatException()
        {
            string exampleDate = "27kd Dec 2012";
            void TestDelegate() => _dateParserHelper.ParseDate(exampleDate);
            Assert.Throws<InvalidDayFormatException>(TestDelegate);
        }
        
        [Test]
        public void Test_ParseDate()
        {
            string exampleDate = "27th December 2012";
            var date = _dateParserHelper.ParseDate(exampleDate);
            Assert.IsTrue(date == new DateTime(2012,12,27));
        }
        
        [Test]
        public void Test_ParseDate_ValidFormat_InValidValue()
        {
            string exampleDate = "42th December 2012";
            void TestDelegate() => _dateParserHelper.ParseDate(exampleDate);
            Assert.Throws<InvalidInputDateFormatException>(TestDelegate);
        }
        
        [Test]
        public void Test_DateParserFactory_ShortDateParser()
        {
            var args = new GetDateParserArgs
            {
                DateString = "27th Dec 2012",
                CultureCode = _dateParserHelperArgs.CultureCode,
                DayFormatRegex = _dateParserHelperArgs.DayFormatRegex
            };

            IDateParser parser = _dateParserFactory.GetDateParser(args);
            
            Assert.IsInstanceOf<ShortMonthDateParser>(parser);
        }        
        
        [Test]
        public void Test_DateParserFactory_LongDateParser()
        {
            var args = new GetDateParserArgs
            {
                DateString = "27th December 2012",
                CultureCode = _dateParserHelperArgs.CultureCode,
                DayFormatRegex = _dateParserHelperArgs.DayFormatRegex
            };

            IDateParser parser = _dateParserFactory.GetDateParser(args);
            
            Assert.IsInstanceOf<LongMonthDateParser>(parser);
        }
    }
}