﻿using System;
using System.Linq;
using GRM.Common.Entities;
using GRM.Common.Infrastructure;
using GRM.Common.Models;
using GRM.Common.Parsers.Data;
using GRM.Repository;
using NUnit.Framework;

namespace GRM.Tests
{
    [TestFixture]
    public class DataParserTests
    {
        [Test]
        public void Text_ParsingPartnerContractEntity()
        {
            
            GetDateParserArgs getDateParserArgs = new GetDateParserArgs
            {
                DayFormatRegex = @"^\d{1,2}((st)|(nd)|(rd)|(th))$",
                CultureCode = "en-GB"
            };
            DataParser<PartnerContract> dataParser = new DataParser<PartnerContract>(getDateParserArgs,new[]{"Partner","Usage"});
            var partnerContract = dataParser.GetEntity("ITunes|digital download");
            Assert.AreEqual("ITunes", partnerContract.Partner);
            Assert.AreEqual("digital download", partnerContract.Usage);
        }
        
        [Test]
        public void Text_ParsingMusicContractEntity()
        {
            GetDateParserArgs getDateParserArgs = new GetDateParserArgs
            {
                DayFormatRegex = @"^\d{1,2}((st)|(nd)|(rd)|(th))$",
                CultureCode = "en-GB",
            };
            DataParser<MusicContract> dataParser = new DataParser<MusicContract>(getDateParserArgs,"Artist|Title|Usages|StartDate|EndDate".Split('|'));
            var musicContract = dataParser.GetEntity("Monkey Claw|Christmas Special|streaming|25st Dec 2012|31st Dec 2012");
            Assert.AreEqual("Monkey Claw", musicContract.Artist);
            Assert.AreEqual("Christmas Special", musicContract.Title);
            Assert.AreEqual(1, musicContract.Usages?.Count());
            Assert.AreEqual(new DateTime(2012,12,25), musicContract.StartDate);
        }
        
        [Test]
        public void Text_ParsingMusicContractEntityWithNullEndDate()
        {
            GetDateParserArgs getDateParserArgs = new GetDateParserArgs
            {
                DayFormatRegex = @"^\d{1,2}((st)|(nd)|(rd)|(th))$",
                CultureCode = "en-GB",
            };
            DataParser<MusicContract> dataParser = new DataParser<MusicContract>(getDateParserArgs,"Artist|Title|Usages|StartDate|EndDate".Split('|'));
            var musicContract = dataParser.GetEntity("Monkey Claw|Christmas Special|streaming|25st Dec 2012|");
            Assert.AreEqual("Monkey Claw", musicContract.Artist);
            Assert.AreEqual("Christmas Special", musicContract.Title);
            Assert.AreEqual(1, musicContract.Usages?.Count());
            Assert.AreEqual(new DateTime(2012,12,25), musicContract.StartDate);
            Assert.AreEqual("25st Dec 2012", musicContract.StartDateString);
            Assert.IsNull(musicContract.EndDate);
        }
        
    }
}