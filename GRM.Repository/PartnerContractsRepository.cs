﻿using System.Collections.Generic;
using System.Linq;
using GRM.Common.Entities;
using GRM.Common.Infrastructure;

namespace GRM.Repository
{
    public class PartnerContractsRepository : BaseRepository<PartnerContract>, IPartnerContractsRepository
    {
        public IEnumerable<PartnerContract> GetByUsage(string usage)
        {
            return DbSet.Where(n => n.Usage == usage);
        }

        public IEnumerable<PartnerContract> GetByPartner(string partnerName)
        {
            return DbSet.Where(n => n.Partner == partnerName);
        }
    }
}