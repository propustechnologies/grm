﻿using System;
using System.Collections.Generic;
using System.Linq;
using GRM.Common.Entities;
using GRM.Common.Infrastructure;

namespace GRM.Repository
{
    public class MusicContractsRepository : BaseRepository<MusicContract>, IMusicContractsRepository
    {
        public IEnumerable<MusicContract> GetByDate(DateTime date)
        {
            return DbSet.Where(n =>
                n.StartDate.Date <= date && 
                (!n.EndDate.HasValue || date <= n.EndDate.Value.Date));
        }
    }
}