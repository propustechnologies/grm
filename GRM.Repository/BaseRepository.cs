﻿using System.Collections.Generic;
using GRM.Common.Data;
using GRM.Common.Entities;
using GRM.Common.Infrastructure;

namespace GRM.Repository
{
    public abstract class BaseRepository<T> : IRepository<T> where T : Entity
    {
        public IEnumerable<T> DbSet => DataContext.GetCollection<T>();
    }
}