﻿using System.Collections.Generic;
using System.Linq;
using GRM.Business;
using GRM.Common.Infrastructure;
using GRM.Common.Messages;
using GRM.Common.Models;
using GRM.Common.Parsers.Date;

namespace GRM.Service
{
    //Common Service Interface.
    public class CommonService : ICommonService
    {
        private readonly IMusicContractBusiness _musicContractBusiness;
        private readonly IPartnerContractBusiness _partnerContractBusiness;
        private IDateParserFactory _dateParserFactory;

        public CommonService()
        {
           _musicContractBusiness = new MusicContractBusiness();
           _partnerContractBusiness = new PartnerContractBusiness();
           _dateParserFactory = new DateParserFactory();
        }

        public IEnumerable<GetMusicContractsByDateAndPartnerResponse> GetMusicContractsByDateAndPartner(GetMusicContractsByDateAndPartnerNameRequest request)
        {
            var args = new GetDateParserArgs
            {
                // this is need for resolving which implementation of DateParser will be resolved.
                DateString = request.DateString,
                CultureCode = "en-GB",
                DayFormatRegex = @"^\d{1,2}((st)|(nd)|(rd)|(th))$"
            };
            
            var date = _dateParserFactory.GetDateParser(args).ParseDate(request.DateString);
            
            var musicContractsQuery = _musicContractBusiness.GetContractsByDate(date);
            var partnerContractQuery = _partnerContractBusiness.GetContractsByPartnerName(request.PartnerName);
            var result = from mc in musicContractsQuery
                            from pc in partnerContractQuery
                            where mc.Usages.Any(x => x.Contains(pc.Usage))
                            select new GetMusicContractsByDateAndPartnerResponse
                            {
                                Artist = mc.Artist,
                                Title = mc.Title,
                                StartDate = mc.StartDateString,
                                EndDate = mc.EndDateString,
                                Usage = pc.Usage
                            };

            return result;
        }
    }
}