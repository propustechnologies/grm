﻿using System.Collections.Generic;
using GRM.Common.Entities;
using GRM.Common.Infrastructure;
using GRM.Repository;

namespace GRM.Business
{
    public class PartnerContractBusiness : IPartnerContractBusiness
    {
        private readonly IPartnerContractsRepository _partnerContractsRepository = new PartnerContractsRepository();

        public IEnumerable<PartnerContract> GetContractsByPartnerName(string partnerName)
        {
            return _partnerContractsRepository.GetByPartner(partnerName);
        }
    }
}