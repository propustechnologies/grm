﻿using System;
using System.Collections.Generic;
using System.Threading;
using GRM.Common.Entities;
using GRM.Common.Infrastructure;
using GRM.Common.Models;
using GRM.Common.Parsers.Date;
using GRM.Repository;

namespace GRM.Business
{
    public class MusicContractBusiness : IMusicContractBusiness
    {
        private readonly IMusicContractsRepository _musicContractsRepository = new MusicContractsRepository();

        public IEnumerable<MusicContract> GetContractsByDate(DateTime requestDate)
        {
            return _musicContractsRepository.GetByDate(requestDate);
        }
    }
}